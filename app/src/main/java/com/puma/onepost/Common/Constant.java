package com.puma.onepost.Common;

/**
 * Created by puma on 4/4/2016.
 */
public class Constant {

    public static final int SPLASH_TIME = 3000;
    public static final int VOLLEY_TIME_OUT = 60000;
    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final String PRODUCT = "product";
    public static final String ORDER = "order";

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
}
