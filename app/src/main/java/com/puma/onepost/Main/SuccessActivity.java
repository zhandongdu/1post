package com.puma.onepost.Main;

import android.content.Intent;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.hanks.htextview.typer.TyperTextView;
import com.puma.onepost.Base.BaseActivity;
import com.puma.onepost.R;
import com.puma.onepost.Widget.CustomButton;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SuccessActivity extends BaseActivity implements View.OnClickListener{

    @Bind(R.id.txvFacebook) TyperTextView txvFacebook;
    @Bind(R.id.txvTwitter) TyperTextView txvTwitter;
    @Bind(R.id.txvInstagram) TyperTextView txvInstagram;

    @Bind(R.id.lytFacebook) LinearLayout lytFacebook;
    @Bind(R.id.lytTwitter) LinearLayout lytTwitter;
    @Bind(R.id.lytInstagram) LinearLayout lytInstagram;

    @Bind(R.id.btnPostMore) CustomButton btnPostMore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        ButterKnife.bind(this);

        loadLayout();
    }

    private void loadLayout(){

        if (OptionActivity.isFacebook && OptionActivity.isTwitter && OptionActivity.isInstagram){

            lytFacebook.setVisibility(View.VISIBLE);
            lytInstagram.setVisibility(View.VISIBLE);
            lytTwitter.setVisibility(View.VISIBLE);
            txvFacebook.animateText(" ..............  ✅" );
            txvTwitter.animateText(" .............. ✅");
            txvInstagram.animateText(" ............... ✅");
        }else if (OptionActivity.isFacebook){

            lytFacebook.setVisibility(View.VISIBLE);
            txvFacebook.animateText(" ..............  ✅" );
        }else if (OptionActivity.isTwitter){

            lytTwitter.setVisibility(View.VISIBLE);
            txvTwitter.animateText("  ............... ✅");
        }else if (OptionActivity.isInstagram){

            lytInstagram.setVisibility(View.VISIBLE);
            txvInstagram.animateText(" ............. ✅");
        }else if (OptionActivity.isInstagram && OptionActivity.isTwitter){

            lytInstagram.setVisibility(View.VISIBLE);
            lytTwitter.setVisibility(View.VISIBLE);
            txvTwitter.animateText(" .............. ✅");
            txvInstagram.animateText(" .............. ✅");
        }else if (OptionActivity.isFacebook && OptionActivity.isTwitter){

            lytFacebook.setVisibility(View.VISIBLE);
            lytTwitter.setVisibility(View.VISIBLE);
            txvFacebook.animateText(" ..............  ✅" );
            txvTwitter.animateText(" .............. ✅");
        }else if (OptionActivity.isFacebook && OptionActivity.isInstagram){

            lytFacebook.setVisibility(View.VISIBLE);
            lytInstagram.setVisibility(View.VISIBLE);
            txvFacebook.animateText(" .............  ✅" );
            txvInstagram.animateText(" .............. ✅");
        }

        btnPostMore.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnPostMore:

                startActivity(new Intent(this, SelectActivity.class));
                finish();
                break;
        }
    }
}
