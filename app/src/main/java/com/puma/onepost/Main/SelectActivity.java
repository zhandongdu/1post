package com.puma.onepost.Main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ayz4sci.androidfactory.permissionhelper.PermissionHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.NativeExpressAdView;
import com.puma.onepost.Base.BaseActivity;
import com.puma.onepost.Common.Common;
import com.puma.onepost.Common.Constant;
import com.puma.onepost.R;
import com.puma.onepost.Utils.BitmapUtils;
import com.puma.onepost.Utils.MediaRealPathUtil;
import com.puma.onepost.Utils.PhotoSelectDialog;
import com.puma.onepost.Widget.CustomButton;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.tajchert.nammu.PermissionCallback;

public class SelectActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.btnCamera) CustomButton btnCamera;
    @Bind(R.id.btnGallery) CustomButton btnGallery;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA, Manifest.permission.MANAGE_DOCUMENTS};

    private Uri _imageCaptureUri;
    private String _photoPath = "";
    public static int MY_PEQUEST_CODE = 123;

    PermissionHelper permissionHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        ButterKnife.bind(this);

        loadLayout();
    }

    private void loadLayout(){

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        btnCamera.setOnClickListener(this);
        btnGallery.setOnClickListener(this);

        permissionHelper = PermissionHelper.getInstance(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnCamera:
//                if (!hasPermissions(this, PERMISSIONS)){
//
//                    ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PEQUEST_CODE);
//                }else {
//
//                    doTakePhotoAction();
//                }

                permissionHelper.verifyPermission(
                        new String[]{"read external storage", "write external storage",  "take picture"},
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        new PermissionCallback() {
                            @Override
                            public void permissionGranted() {
                                //action to perform when permission granteed

                                doTakePhotoAction();
                            }

                            @Override
                            public void permissionRefused() {
                                //action to perform when permission refused
                            }
                        }
                );
                break;
            case R.id.btnGallery:

//                if (!hasPermissions(this, PERMISSIONS)){
//
//                    ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PEQUEST_CODE);
//                }else {
//
//                   doTakeGallery();
//                }

                permissionHelper.verifyPermission(
                        new String[]{"read external storage", "write external storage",  "take picture"},
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        new PermissionCallback() {
                            @Override
                            public void permissionGranted() {
                                //action to perform when permission granteed

                                doTakeGallery();
                            }

                            @Override
                            public void permissionRefused() {
                                //action to perform when permission refused
                            }
                        }
                );
                break;
        }
    }

    public void doTakePhotoAction(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        _photoPath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
//        _imageCaptureUri = Uri.fromFile(new File(_photoPath));
        _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(_photoPath));

        Common._imageCaptureUri = _imageCaptureUri;

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constant.PICK_FROM_CAMERA);
    }

    public void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, Constant.PICK_FROM_ALBUM);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

        }
    }

    @Override
    protected void onDestroy() {
        permissionHelper.finish();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        permissionHelper.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){

            case Constant.PICK_FROM_CAMERA:

                if (resultCode == RESULT_OK){
                    try {

                        String filename = "IMAGE_" + System.currentTimeMillis() + ".jpg";

                        Bitmap bitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);

//                        String w_strFilePath = "";
//                        String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(bitmap, filename);
//                        if (w_strLimitedImageFilePath != null) {
//                            w_strFilePath = w_strLimitedImageFilePath;
//                        }

//                        _photoPath = w_strFilePath;

                        Common._photoPath = _photoPath;
                        Common.bitmap = bitmap;

                        if (!_photoPath.isEmpty()){

                            gotoOption();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case Constant.PICK_FROM_ALBUM:

                Uri w_uri = null;

                try {

                    w_uri = data.getData();
                }catch (Exception e){
                    e.printStackTrace();
                }

                if (w_uri != null){
                    Common._imageCaptureUri = w_uri;

                    String w_strPath = MediaRealPathUtil.getPath(this, w_uri);

                    if (w_strPath == null) {

                        return;
                    }
                    String filename = Common.fileNameWithoutExtFromUrl(w_strPath) + ".jpg";

                    Bitmap bitmap = BitmapUtils.loadOrientationAdjustedBitmap(w_strPath);

                    String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(bitmap, filename);
                    if (w_strLimitedImageFilePath != null) {
                        w_strPath = w_strLimitedImageFilePath;
                    }

//                imgPhoto.setImageBitmap(bitmap);
                    _photoPath = w_strPath;
                    Common._photoPath = _photoPath;
                    Common.bitmap = bitmap;

                    if (!_photoPath.isEmpty()){

                        gotoOption();
                    }
                }



                break;
        }
    }

    private void gotoOption(){

        Intent intent = new Intent(this, OptionActivity.class);
        startActivity(intent);
        finish();
    }
}
