package com.puma.onepost.Main;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.puma.onepost.Base.BaseActivity;
import com.puma.onepost.Common.Common;
import com.puma.onepost.Common.Constant;
import com.puma.onepost.R;
import com.puma.onepost.Utils.BitmapUtils;
import com.puma.onepost.Utils.MediaRealPathUtil;
import com.puma.onepost.Utils.PhotoSelectDialog;
import com.puma.onepost.Widget.CustomButton;
import com.puma.onepost.Widget.CustomEditText;
import com.puma.onepost.Widget.MaterialDesignIconsTextView;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OptionActivity extends BaseActivity implements View.OnClickListener{

    private static final String PERMISSION = "publish_actions";
    @Bind(R.id.btnQuick) CustomButton btnQuick;
    @Bind(R.id.edtComment) CustomEditText edtComment;
    @Bind(R.id.imgPhoto) ImageView imgPhoto;
    @Bind(R.id.imgClip) ImageView imgClip;
    ShareDialog shareDialog;

    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onCancel() {
            Log.d("HelloFacebook", "Canceled");
        }

        @Override
        public void onError(FacebookException error) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
            String title = getString(R.string.error);
            String alertMessage = error.getMessage();
            showResult(title, alertMessage);
        }

        @Override
        public void onSuccess(Sharer.Result result) {
            Log.d("HelloFacebook", "Success!");
            if (result.getPostId() != null) {
//                String title = getString(R.string.success);
//                String id = result.getPostId();
//                String alertMessage = getString(R.string.successfully_posted_post, id);
//                showResult(title, alertMessage);
            }
        }

        private void showResult(String title, String alertMessage) {
            new android.app.AlertDialog.Builder(OptionActivity.this)
                    .setTitle(title)
                    .setMessage(alertMessage)
                    .setPositiveButton(R.string.ok, null)
                    .show();
        }
    };

    Bitmap bitmap;

    TwitterAuthClient mTwitterAuthClient = new TwitterAuthClient();

    private int SUCCESS_TWITTER = 123;
    private int SUCCESS_INSTAGRAM = 234;

    private CallbackManager callbackManager;
    private LoginManager manager;
    private PendingAction pendingAction = PendingAction.NONE;
    private boolean canPresentShareDialogWithPhotos;

    public static boolean isFacebook = false;
    public static  boolean isTwitter = false;
    public static boolean isInstagram = false;

    InterstitialAd _interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handlePendingAction();

                    }

                    @Override
                    public void onCancel() {
                        if (pendingAction != PendingAction.NONE) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (pendingAction != PendingAction.NONE
                                && exception instanceof FacebookAuthorizationException) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }

                    }

                    private void showAlert() {
                        new android.app.AlertDialog.Builder(OptionActivity.this)
                                .setTitle(R.string.cancelled)
                                .setMessage(R.string.permission_not_granted)
                                .setPositiveButton(R.string.ok, null)
                                .show();
                    }
                });

        canPresentShareDialogWithPhotos = ShareDialog.canShow(
                SharePhotoContent.class);

        setContentView(R.layout.activity_option);

        _interstitialAd = new InterstitialAd(this);
        _interstitialAd.setAdUnitId("ca-app-pub-1342869864035043/4457998808");

        _interstitialAd.loadAd(new AdRequest.Builder().build());



        ButterKnife.bind(this);

        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(
                callbackManager,
                shareCallback);

        loadLayout();

        isTwitter = false;
        isFacebook = false;
        isTwitter = false;
    }

    private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }

    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case NONE:
                break;
            case POST_PHOTO:
                postPhoto();
                break;
            case POST_STATUS_UPDATE:

                break;
        }
    }

    private void loadLayout(){

        btnQuick.setOnClickListener(this);
        imgClip.setOnClickListener(this);

        bitmap = BitmapUtils.loadOrientationAdjustedBitmap(Common._photoPath);

        if (Common.bitmap != null){

            imgPhoto.setImageBitmap(Common.bitmap);
        }

//        TwitterConfig config = new TwitterConfig.Builder(this)
//                .logger(new DefaultLogger(Log.DEBUG))
//                .twitterAuthConfig(new TwitterAuthConfig(getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))
//                .debug(true)
//                .build();
//        Twitter.initialize(config);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btnQuick:

                if (_interstitialAd.isLoaded()){
                    _interstitialAd.show();

                }else {
                    showDialog();
                }


                break;
            case R.id.imgClip:

                if (!edtComment.getText().toString().isEmpty()){

                    copyToClipBoard();
                }
                break;
        }
    }

    private void copyToClipBoard(){

        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Comment", edtComment.getText().toString());
        clipboard.setPrimaryClip(clip);
        showToast("Copied to clipboard!");
    }

    private void postPhoto() {

        SharePhoto sharePhoto = new SharePhoto.Builder().
                setBitmap(bitmap).
                setCaption(edtComment.getText().toString())
                .build();

        ArrayList<SharePhoto> photos = new ArrayList<>();
        photos.add(sharePhoto);

        SharePhotoContent sharePhotoContent =
                new SharePhotoContent.Builder().setPhotos(photos).build();
        if (canPresentShareDialogWithPhotos) {
            shareDialog.show(sharePhotoContent);
        } else if (hasPublishPermission()) {
            ShareApi.share(sharePhotoContent, shareCallback);
        } else {
            pendingAction = PendingAction.POST_PHOTO;
            // We need to get new permissions, then complete the action when we get called back.
            LoginManager.getInstance().logInWithPublishPermissions(
                    this,
                    Arrays.asList(PERMISSION));
        }
    }

    private boolean hasPublishPermission() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && accessToken.getPermissions().contains("publish_actions");
    }


    private void shareFacebook(){

        FacebookSdk.sdkInitialize(getApplicationContext());

            callbackManager = CallbackManager.Factory.create();

            List<String> permissionNeeds = Arrays.asList("publish_actions");

            manager = LoginManager.getInstance();

            manager.logInWithPublishPermissions(this, permissionNeeds);
            manager.registerCallback(callbackManager, new  FacebookCallback<LoginResult>()
            {
                @Override
                public void onSuccess(LoginResult loginResult)
                {
                    shareToFacebook();
                }

                @Override
                public void onCancel()
                {
                    System.out.println("onCancel");
                }

                @Override
                public void onError(FacebookException exception)
                {
                    System.out.println("onError");
                }
            });
    }

    private void shareToFacebook(){
//
//        SharePhoto photo = new SharePhoto.Builder()
//                .setBitmap(bitmap)
//                .setCaption(edtComment.getText().toString())
//                .build();
//        SharePhotoContent content = new SharePhotoContent.Builder()
//                .addPhoto(photo)
//                .build();
//
//        ShareApi.share(content, null);

//        Uri uri = Uri.fromFile(new File(Common._photoPath));
//        ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
//                .setContentDescription("Your Description")
//                .setImageUrl(uri)
//                .build();
//
//        shareDialog.show(shareLinkContent, ShareDialog.Mode.FEED);
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,    Intent data)
    {
        super.onActivityResult(requestCode, responseCode, data);

        if (isFacebook){

            callbackManager.onActivityResult(requestCode, responseCode, data);
        }

        Log.d("response==>", String.valueOf(responseCode));
        if (responseCode == Activity.RESULT_OK){

            if (isTwitter && isInstagram){

                shareToTwitter();
            }else if (isTwitter){

                shareToTwitter();
            }else if (isInstagram){

                shareinstagram();
            }else {

                startActivity(new Intent(this, SuccessActivity.class));
                finish();
            }
        }
        if (requestCode == SUCCESS_TWITTER){
            if (responseCode == RESULT_CANCELED){


                if (isInstagram){
                    shareinstagram();
                }else {
                    startActivity(new Intent(this, SuccessActivity.class));
                    finish();
                }

            }
        }
        if (requestCode == SUCCESS_INSTAGRAM){
            if (responseCode == RESULT_CANCELED){


                startActivity(new Intent(this, SuccessActivity.class));
                finish();
            }
        }

        mTwitterAuthClient.onActivityResult(requestCode, responseCode, data);
    }

    private void shareToTwitter(){

        final Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(Common._photoPath));

        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, edtComment.getText().toString());
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setType("image/jpeg");
            intent.setPackage("com.twitter.android");
            startActivityForResult(intent, SUCCESS_TWITTER);
        }catch (Exception e){
            e.printStackTrace();
            isTwitter = false;
        }

    }

    public void shareinstagram(){

        try{
//            Uri file = Common._imageCaptureUri;
            Uri _imageCaptureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(Common._photoPath));
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, _imageCaptureUri);
            shareIntent.putExtra(Intent.EXTRA_TITLE, edtComment.getText().toString());
            shareIntent.setPackage("com.instagram.android");
            startActivityForResult(shareIntent, SUCCESS_INSTAGRAM);
        }catch (Exception e) {
            e.printStackTrace();
            isInstagram = false;
        }

    }

    private void showDialog(){

        isTwitter = false;
        isFacebook = false;
        isInstagram = false;

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View dialogView = getLayoutInflater().inflate(R.layout.dialog, null);

        final RelativeLayout rytFacebook = (RelativeLayout) dialogView.findViewById(R.id.rytFacebook);
        final MaterialDesignIconsTextView checkFacebook = (MaterialDesignIconsTextView) dialogView.findViewById(R.id.checkFacebook);

        final RelativeLayout rytTwitter = (RelativeLayout) dialogView.findViewById(R.id.rytTwitter);
        final MaterialDesignIconsTextView checkTwitter = (MaterialDesignIconsTextView) dialogView.findViewById(R.id.checkTwitter);

        final RelativeLayout rytInstagram = (RelativeLayout) dialogView.findViewById(R.id.rytInstagram);
        final MaterialDesignIconsTextView checkInstagram = (MaterialDesignIconsTextView)dialogView.findViewById(R.id.checkInstagram);

        final RelativeLayout rytSnapchat = (RelativeLayout) dialogView.findViewById(R.id.rytSnap);
        final MaterialDesignIconsTextView checkSnap = (MaterialDesignIconsTextView) dialogView.findViewById(R.id.checkSnap);
        final Button btnOk = (Button)dialogView.findViewById(R.id.btnOk);

        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();

        dialog.show();


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if (isFacebook){

                    performPublish(PendingAction.POST_PHOTO, canPresentShareDialogWithPhotos);

                }else if(isTwitter ){

                    shareToTwitter();

                }else if (isInstagram){

                    shareinstagram();
                }

            }
        });

        rytFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkFacebook.getText() == getString(R.string.material_icon_check_empty)){

                    checkFacebook.setText(getString(R.string.material_icon_check_full));
                    isFacebook = true;

                }else {
                    checkFacebook.setText(getString(R.string.material_icon_check_empty));
                    isFacebook = false;
                }
            }
        });

        rytTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkTwitter.getText() == getText(R.string.material_icon_check_empty)){

                    checkTwitter.setText(getString(R.string.material_icon_check_full));
                    isTwitter = true;
                }else {
                    checkTwitter.setText(getString(R.string.material_icon_check_empty));
                    isTwitter = false;
                }

            }
        });

        rytInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInstagram.getText() == getText(R.string.material_icon_check_empty)){

                    checkInstagram.setText(getString(R.string.material_icon_check_full));
                    isInstagram = true;
                }else {
                    checkInstagram.setText(getString(R.string.material_icon_check_empty));
                    isInstagram = false;
                }
            }
        });
    }

    private void performPublish(PendingAction action, boolean allowNoToken) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null || allowNoToken) {
            pendingAction = action;
            handlePendingAction();
        }
    }
}


