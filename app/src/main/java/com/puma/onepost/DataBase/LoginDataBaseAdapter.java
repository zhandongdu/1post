package com.puma.onepost.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLDataException;

/**
 * Created by Ung Man Pak on 12/31/2015.
 */
public class LoginDataBaseAdapter {

    static final String DATABASE_NAME = "login.db";
    static final String TABLE_NAME = "LOGIN";
    static final int DATABASE_VERSION = 1;
    public static final int NAME_COLUMN = 1;

    // SQL Statement to create a new database.
    static final String DATABASE_CREATE = "create table " + TABLE_NAME + "( " + "ID" + " integer primary key autoincrement,"+ "EMAIL  text,PASSWORD text); ";

    // Variable to hold the database instance.
    public SQLiteDatabase db;

    // Context of teh application using database.
    private final Context context;

    // DataBase open/upgrade helper.
    private DataBaseHelper dbHelper;

    public LoginDataBaseAdapter(Context context) {
        this.context = context;
        dbHelper = new DataBaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public LoginDataBaseAdapter open()throws SQLDataException{
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        db.close();
    }

    public SQLiteDatabase getDataBaseInstance(){
        return db;
    }

    public void insertEntry(String email, String password){

        ContentValues newValues = new ContentValues();
        // Assign values for each row
        newValues.put("EMAIL",email);
        newValues.put("PASSWORD",password);

        // Insert the row into your table
        db.insert(TABLE_NAME, null, newValues);
    }

    public int deleteEntry(String email){

        String where = "EMAIL=?";
        int numberOfEntriesDeleted = db.delete(TABLE_NAME,where,new String[]{email});
        return numberOfEntriesDeleted;
    }

    public String getSingleEntry(String email){

        Cursor cursor = db.query(TABLE_NAME, null, "EMAIL=?", new String[]{email}, null, null, null);

        if (cursor.getCount()<1){ //userName not exist.

            cursor.close();
            return "Not Exist";
        }
        cursor.moveToFirst();
        String password = cursor.getString(cursor.getColumnIndex("PASSWORD"));
        cursor.close();
        return password;
    }

    public void UpdateEntry(String email, String password){

        // Define the updated row content
        ContentValues updatedValues = new ContentValues();

        //Assign values for each row.
        updatedValues.put("EMAIL", email);
        updatedValues.put("PASSWORD", password);

        String where = "EMAIL = ?";
        db.update(TABLE_NAME,updatedValues,where,new String[]{email});
    }


}
