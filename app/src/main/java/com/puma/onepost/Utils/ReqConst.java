package com.puma.onepost.Utils;

/**
 * Created by puma on 4/5/2016.
 */
public class ReqConst {

    public static final String SERVER_ADDR = "http://52.36.137.16";

    public static final String SERVER_URL = SERVER_ADDR + "/index.php/api/";

    public static final String REQ_LOGIN = "login";

    //request params
    public static final String PARAM_ID = "id";

    //response value
    public static final String RES_CODE = "result_code";


    public static final int CODE_SUCCESS = 0;
    public static final int CODE_UNREGUSER = 101;
    public static final int CODE_INVALIDPWD = 102;
    public static final int CODE_INVALIDEMAIL = 104;
    public static final int CODE_UNAUTHEMAIL = 105;
    public static final int CODE_AUTHFAIL = 106;
    public static final int CODE_INVALIDAUTH = 107;
    public static final int CODE_RESET_UNAUTHEMAIL = 108;
    public static final int CODE_OVERFLOW = 111;


    public static final int CODE_PROFILE = 202;
    public static final int CODE_UNREGISTERDROOM = 201;

}
